from django.db import models

# Create your models here.


class Reportero(models.Model):
    nombre = models.CharField(max_length=30)
    apellido = models.CharField(max_length=30)
    correo = models.EmailField()

    def __str__(self):
        return "%s %s" % (self.nombre, self.apellido)


class Articulo(models.Model):
    encabezado = models.CharField(max_length=100)
    fecha_pub = models.DateField()
    reportero = models.ForeignKey(Reportero, on_delete=models.CASCADE)

    def __str__(self):
        return self.encabezado

    class Meta:
        ordering = ['encabezado']
