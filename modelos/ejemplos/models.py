"""
https://docs.djangoproject.com/en/3.1/topics/db/managers/
https://docs.djangoproject.com/en/3.1/topics/db/models/
https://docs.djangoproject.com/en/3.1/ref/models/instances/
"""

from django.db import models

# Crear los modelos aquí.


class Perro(models.Model):
    nombre = models.CharField(max_length=20)


class Persona(models.Model):
    nombre = models.CharField(max_length=30)
    apellido = models.CharField(max_length=30)
    fecha_nacimiento = models.DateField()

    # Métodos de modelo
    # Añadir funcionalidad a nivel de fila
    def estado_milenial(self):
        """Devuelve el estado milenial de una persona."""
        import datetime
        if self.fecha_nacimiento < datetime.date(1981, 6, 1):
            return "Pre-milenial"
        elif self.fecha_nacimiento < datetime.date(1996, 12, 1):
            return "Milenial"
        else:
            return "Post-milenial"

    @property
    def nombre_completo(self):
        """Devuelve el nombre completo de la persona."""
        return '%s %s' % (self.nombre, self.apellido)


# Métodos de clases
# Añadir funcionalidad a nivel de tabla
class Revista(models.Model):
    nombre = models.CharField(max_length=50)

    @classmethod
    def crear(cls, nombre):
        revista = cls(nombre=nombre)
        # Hacer algo con la revista
        return revista


# Directores
# Añadir funcionalidad a nivel de tabla
class LibroManager(models.Manager):
    def crear_libro(self, titulo):
        libro = self.create(titulo=titulo)
        # Hacer algo con el libro
        return libro


class Libro(models.Model):
    titulo = models.CharField(max_length=100)

    objects = LibroManager()
