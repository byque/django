from .models import Pregunta

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

import datetime


class PruebaModeloPregunta(TestCase):
    def test_fue_publicada_recien_pregunta_futura(self):
        """
        fue_publicada_recien() para preguntas cuya fecha_pub esté en el futuro.
        :return: False
        """
        tiempo = timezone.now().date() + datetime.timedelta(days=30)
        pregunta_futura = Pregunta(fecha_pub=tiempo)
        self.assertIs(pregunta_futura.fue_publicada_recien(), False)

    def test_fue_publicada_recien_pregunta_antigua(self):
        """
        fue_publicada_recien() para preguntas cuya fecha_pub es más antigua que 1 día.
        :return: False

        TODO: Revisar el error de <can't compare datetime.datetime to datetime.date>, código actual solo funciona
              con días y no con horas o segundos.
        """
        tiempo = timezone.now().date() - datetime.timedelta(days=2)
        pregunta_antigua = Pregunta(fecha_pub=tiempo)
        self.assertIs(pregunta_antigua.fue_publicada_recien(), False)

    def test_fue_publicada_recien_pregunta_reciente(self):
        """
        fue_publicada_recien() para preguntas cuya fecha_pub está dentro del último día.
        :return: True
        """
        tiempo = timezone.now().date() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        pregunta_reciente = Pregunta(fecha_pub=tiempo)
        self.assertIs(pregunta_reciente.fue_publicada_recien(), True)


def crear_pregunta(texto_pregunta, dias):
    """
    Crear una pregunta con el texto 'texto_pregunta' y publicado el  número de 'dias' compensados a ahora
    (negativo para preguntas publicadas en el pasado, positiva para preguntas que serán publicadas).
    """
    tiempo = timezone.now() + datetime.timedelta(days=dias)
    return Pregunta.objects.create(texto_pregunta=texto_pregunta, fecha_pub=tiempo)


class PruebasPreguntaVistaIndice(TestCase):
    def test_sin_preguntas(self):
        """Si no existen preguntas, un mensaje apropiado es mostrado"""
        respuesta = self.client.get(reverse('encuestas:indice'))
        self.assertEqual(respuesta.status_code, 200)
        self.assertContains(respuesta, "No hay encuestas disponibles.")
        self.assertQuerysetEqual(respuesta.context['lista_preguntas_recientes'], [])

    def test_pregunta_pasada(self):
        """Preguntas con una fecha_pub en el pasado son mostradas en la página índice."""
        crear_pregunta(texto_pregunta="Pregunta pasada.", dias=-30)
        respuesta = self.client.get(reverse('encuestas:indice'))
        self.assertQuerysetEqual(
            respuesta.context['lista_preguntas_recientes'],
            ['<Pregunta: Pregunta pasada.>']
        )

    def test_pregunta_futura(self):
        """Preguntas con una fecha_pub en el futuro no son mostradas en la página índice."""
        crear_pregunta(texto_pregunta="Pregunta futura.", dias=30)
        respuesta = self.client.get(reverse('encuestas:indice'))
        self.assertContains(respuesta, "No hay encuestas disponibles.")
        self.assertQuerysetEqual(respuesta.context['lista_preguntas_recientes'], [])

    def test_pregunta_futura_y_pregunta_pasada(self):
        """Aunque existan tanto preguntas pasadas como futuras, solo las preguntas pasadas se muestran."""
        crear_pregunta(texto_pregunta="Pregunta pasada.", dias=-30)
        crear_pregunta(texto_pregunta="Pregunta futura.", dias=30)
        respuesta = self.client.get(reverse('encuestas:indice'))
        self.assertQuerysetEqual(
            respuesta.context['lista_preguntas_recientes'],
            ['<Pregunta: Pregunta pasada.>']
        )

    def test_dos_preguntas_pasadas(self):
        """La página índice de preguntas puede mostrar múltiples preguntas."""
        crear_pregunta(texto_pregunta="Pregunta pasada 1.", dias=-30)
        crear_pregunta(texto_pregunta="Pregunta pasada 2.", dias=-5)
        respuesta = self.client.get(reverse('encuestas:indice'))
        self.assertQuerysetEqual(
            respuesta.context['lista_preguntas_recientes'],
            ['<Pregunta: Pregunta pasada 2.>', '<Pregunta: Pregunta pasada 1.>']
        )


class PruebasPreguntaVistaDetalle(TestCase):
    def test_pregunta_futura(self):
        """La vista detalle de una pregunta con una fecha_pub en el futuro devuelve un 404 no encontrado."""
        pregunta_futura = crear_pregunta(texto_pregunta='Pregunta futura.', dias=5)
        url = reverse('encuestas:detalle', args=(pregunta_futura.id,))
        respuesta = self.client.get(url)
        self.assertEqual(respuesta.status_code, 404)

    def test_pregunta_pasada(self):
        pregunta_pasada = crear_pregunta(texto_pregunta='Pregunta Pasada.', dias=-5)
        url = reverse('encuestas:detalle', args=(pregunta_pasada.id,))
        respuesta = self.client.get(url)
        self.assertContains(respuesta, pregunta_pasada.texto_pregunta)
