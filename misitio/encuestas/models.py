from django.db import models
from django.utils import timezone

import datetime


class Pregunta(models.Model):
    texto_pregunta = models.CharField(max_length=200)
    fecha_pub = models.DateField('Fecha de Publicación')

    def __str__(self):
        return self.texto_pregunta

    def fue_publicada_recien(self):
        ahora = timezone.now().date()
        return ahora - datetime.timedelta(days=1) <= self.fecha_pub <= ahora


class Eleccion(models.Model):
    pregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE)
    texto_eleccion = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)

    def __str__(self):
        return self.texto_eleccion
