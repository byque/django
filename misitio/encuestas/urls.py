from django.urls import path
from . import views

app_name = 'encuestas'
urlpatterns = [
    # ej: /encuestas/
    path('', views.VistaIndice.as_view(), name='indice'),
    # ej: /encuestas/5/
    path('<int:pk>/', views.VistaDetalle.as_view(), name='detalle'),
    # ej: /encuestas/5/resultados/
    path('<int:pk>/resultados/', views.VistaResultados.as_view(), name='resultados'),
    # ej: /encuestas/5/voto/
    path('<int:id_pregunta>/voto/', views.voto, name='voto'),
]
