from .models import Eleccion, Pregunta

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views import generic


class VistaIndice(generic.ListView):
    context_object_name = 'lista_preguntas_recientes'
    template_name = 'encuestas/index.html'

    def get_queryset(self):
        """Devuelve las últimas cinco preguntas publicadas."""
        return Pregunta.objects.filter(
            fecha_pub__lte=timezone.now()
        ).order_by('-fecha_pub')[:5]


class VistaDetalle(generic.DetailView):
    model = Pregunta
    template_name = 'encuestas/detalle.html'

    def get_queryset(self):
        """Excluye cualquier pregunta que no se haya publicado todavía."""
        return Pregunta.objects.filter(fecha_pub__lte=timezone.now())


class VistaResultados(generic.DetailView):
    model = Pregunta
    template_name = 'encuestas/resultados.html'


def voto(peticion, id_pregunta):
    pregunta = get_object_or_404(Pregunta, pk=id_pregunta)
    try:
        eleccion_seleccionada = pregunta.eleccion_set.get(pk=peticion.POST['eleccion'])
    except(KeyError, Eleccion.DoesNotExist):
        return render(peticion, 'encuestas/detalle.html', {
            'pregunta': pregunta,
            'mensaje_error': "No seleccionaste una opción."
        })
    else:
        eleccion_seleccionada.votos += 1
        eleccion_seleccionada.save()
        # Siempre devolver una HttpResponseRedirect después de manipular exitosamente datos POST. Esto previene que los
        # datos sean publicados dos veces si un usuario da click en el botón Atrás.
        return HttpResponseRedirect(reverse('encuestas:resultados', args=(pregunta.id,)))
